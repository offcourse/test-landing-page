(ns app.templates.layout)

(defn title-section []
  [:div.ui.inverted.segment
   [:div.ui.container
    [:h1 "Offcourse"]]])

(defn what-section []
  [:div.ui.padded.segment
   [:a.ui.fluid.image {:href "http://platform.offcourse.io"}
    [:img {:src "offcourse.png"}]]
    [:h1.ui.small.header "Participate in an open-source community and improve your skills as a developer"]])

(defn why-section []
  [:div.ui.segment
    [:h1.ui.centered.header "Why Offcourse?"]
   [:div.ui.horizontal.stackable.segments
    [:div.ui.center.aligned.segment
     [:h2.ui.icon.header
      [:i.circular.trophy.icon]
       "Improve"]
     [:p "Enhance your skills as a developer by contributing to open-source projects"]]
    [:div.ui.center.aligned.segment
     [:h2.ui.icon.header
      [:i.circular.book.icon]
      "Learn"]
     [:p "Receive feedback from coaches and get guidance throughout the project"]]
    [:div.ui.center.aligned.segment
     [:h2.ui.icon.header
      [:i.circular.users.icon]
       "Share"]
      [:p "Share your work with others and show your skills to future employers"]]]])

(defn goal-section []
  [:div.ui.segment
   [:div.ui.embed {:data-source "vimeo"
                   :data-id "595309894"
                   :data-placeholder "https://i.vimeocdn.com/video/595309894.webp?mw=1500&mh=844&q=70"}]
   [:p "Our goal is to make learning as simple as possible and accessible to everyone"]])

(defn mission-section []
   [:div.ui.center.aligned.very.padded.segment
       [:h1.ui.header "Mission"]
       [:p "We envision a society where education is democratized. One where all knowledge is equally accessible to everyone. One where every individual is able to learn smarter together with others. It is our mission to bring this community driven education to learners around the globe."]])


(defn head []
  [:head
   [:meta {:charset "utf-8"}]
   [:meta {:name    "viewport"
           :content "width=device-width, initial-scale=1.0"}]
   [:link {:rel "stylesheet"
           :type "text/css"
           :href "semantic.min.css"}]])


(defn body []
  [:body
   (title-section)
   [:div.ui.container
    [:div.ui.basic.segments
     (what-section)
     (why-section)
     (goal-section)
     (mission-section)]]])

(defn layout []
  [:html
   (head)
   (body)])
