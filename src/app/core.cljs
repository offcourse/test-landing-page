(ns app.core
(:require [cljs.nodejs :as node]
          [app.parser :as parse]
          [app.templates.layout :refer [layout]]
          [reagent.core :as reagent]))

(node/enable-util-print!)

(def fs (node/require "fs"))

(defn ^:export render-page [options]
  (reagent/render-to-static-markup [layout options]))

(def lorem "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.  Donec hendrerit tempor tellus.  Donec pretium posuere tellus.  Proin quam nisl, tincidunt et, mattis eget, convallis nec, purus.  Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.  Nulla posuere.  Donec vitae dolor.  Nullam tristique diam non turpis.  Cras placerat accumsan nulla.  Nullam rutrum.  Nam vestibulum accumsan nisl.")

(defn -main []
  (let [#_options #_{:input  ["-i, --input [file]" "input [file]" "file"]
                     :pinput ["-p, --pinput [file]" "input [file]" "file"]}
        args      {:what lorem
                   :why lorem
                   :how lorem} #_(parse/args options)]
    (.writeFileSync fs "dist/index.html"  (render-page args))))

(set! *main-cli-fn* -main)
