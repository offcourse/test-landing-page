(set-env!
 :resource-paths #{"src" "dist"}
 :dependencies  '[[adzerk/boot-cljs            "1.7.228-2"       :scope "test"]
                  [adzerk/boot-cljs-repl       "0.3.3"           :scope "test"]
                  [adzerk/boot-reload          "0.4.13"          :scope "test"]
                  [crisptrutski/boot-cljs-test "0.3.0"           :scope "test"]
                  [boot-codox                  "0.10.2"          :scope "test"]
                  [weasel                      "0.7.0"           :scope "test"]
                  [org.clojure/tools.nrepl     "0.2.12"          :scope "test"]
                  [com.cemerick/piggieback     "0.2.2-SNAPSHOT"  :scope "test"]
                  [reagent                     "0.6.0"]
                  [org.clojure/clojure         "1.9.0-alpha14"]
                  [org.clojure/core.async      "0.2.395"]
                  [org.clojure/test.check      "0.9.0"]
                  [deraen/boot-livereload      "0.1.2"]
                  [org.clojure/clojurescript   "1.9.293"]
                  [pandeiro/boot-http          "0.7.6"]
                  [offcourse/shared            "0.5.9"]])


(require
 '[adzerk.boot-cljs       :refer [cljs]]
 '[adzerk.boot-cljs-repl  :refer [cljs-repl start-repl]]
 '[deraen.boot-livereload :refer [livereload]]
 '[pandeiro.boot-http     :refer [serve]]
 '[codox.boot             :refer [codox]])

(deftask build []
  (task-options! cljs   {:compiler-options {:optimizations :simple
                                            :target :nodejs}})
  (comp (cljs)
        (target)))

(deftask dev []
  (comp (watch)
        (serve)
        (livereload)
        (checkout)
        (speak)
        (cljs-repl)
        (build)))
